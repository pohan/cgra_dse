import os
import subprocess
from pathlib import Path
import argparse


DEBUG = False


def issue_cmd(cmd):
    cmd_str = " ".join(cmd)
    if DEBUG:
        print(cmd_str)
    else:
        subprocess.check_call(args=[cmd_str], shell=True)


def get_docker_image(container_img):
    cmd = ["docker", "pull", container_img]
    issue_cmd(cmd)


def create_container(container_name, container_img):
    cmd = ["docker", "run"]
    cmd += ["-it", "--rm", "-d"]
    cmd += ["-v", "/cad:/cad"]
    cmd += ["--name", container_name]
    cmd += [container_img, "bash"]
    issue_cmd(cmd)


def copy_to_container(container_name, fr, to):
    cmd = ["docker", "cp", fr, f"{container_name}:{to}"]
    issue_cmd(cmd)


def copy_from_container(container_name, fr, to):
    cmd = ["docker", "cp", f"{container_name}:{fr}", to]
    issue_cmd(cmd)


def docker_exec(container_name, exe_cmd, wdir="/aha/cgra_dse"):
    # environment
    aha_env = "PATH=/aha/bin"
    aha_env += ":/usr/local/sbin"
    aha_env += ":/usr/local/bin"
    aha_env += ":/usr/sbin"
    aha_env += ":/usr/bin"
    aha_env += ":/sbin"
    aha_env += ":/bin"
    # command
    cmd = ["docker", "exec", "-it"]
    cmd += ["-w", wdir]
    cmd += ["--env", aha_env]
    cmd += [container_name]
    cmd += exe_cmd
    issue_cmd(cmd)


def docker_tmux_create(container_name, session_name, window_name="bash", tmux_dir="/aha/cgra_dse/benchmark", detached=True):
    exe_cmd = ["tmux", "new-session"]
    exe_cmd += ["-s", session_name]
    exe_cmd += ["-n", window_name]
    exe_cmd += ["-c", tmux_dir]
    if detached:
        exe_cmd += ["-d"]
    docker_exec(container_name, exe_cmd)


def docker_tmux_new_window(container_name, session_name, window_name="bash", tmux_dir="/aha/cgra_dse/benchmark"):
    exe_cmd = ["tmux", "new-window"]
    exe_cmd += ["-t", session_name]
    exe_cmd += ["-n", window_name]
    exe_cmd += ["-c", tmux_dir]
    docker_exec(container_name, exe_cmd)


def docker_tmux_exec(container_name, session_name, window_name, tmux_exe_cmd):
    exe_cmd = ["tmux", "send"]
    exe_cmd += ["-t", f"{session_name}:{window_name}.0"]
    exe_cmd += [tmux_exe_cmd]
    exe_cmd += ["Enter"]
    docker_exec(container_name, exe_cmd)


def docker_git_checkout(container_name, repo, checkout):
    docker_exec(container_name, ["git", "fetch"], wdir=repo)
    docker_exec(container_name, ["git", "checkout", checkout], wdir=repo)


def docker_git_checkout_file(container_name, path, branch, file_name):
    docker_exec(container_name, ["git", "fetch"], wdir=path)
    docker_exec(container_name, ["git", "checkout", branch, "--", file_name], wdir=path)


def docker_setup_container(container_name, args):
    # setup
    get_docker_image(args.container_image)
    create_container(container_name, args.container_image)
    # copy things into container
    this_folder = Path(__file__).parent.absolute()
    ssh_folder = Path(f"/home/{os.getlogin()}/.ssh").absolute()
    copy_to_container(container_name, str(this_folder), "/aha")
    copy_to_container(container_name, str(ssh_folder), "/root")
    # init container
    docker_exec(container_name, ["apt", "update"])
    docker_exec(container_name, ["apt", "install", "-y", "tmux", "vim"])
    docker_exec(container_name, ["git", "config", "--global", "user.email", args.github_email])
    docker_exec(container_name, ["git", "config", "--global", "user.name", args.github_name])
    docker_exec(container_name, ["pip", "install", "Pillow-SIMD"])
    docker_exec(container_name, ["pip", "install", "pycodestyle"])
    docker_exec(container_name, ["pip", "install", "pandas"])
    docker_exec(container_name, ["cp", "./dotfiles/.tmux.conf", "/root"])
    docker_exec(container_name, ["cp", "./dotfiles/.vimrc", "/root"])
    # create tmux session in container
    docker_tmux_create(container_name, session_name=args.tag, window_name="gaussian")
    docker_tmux_new_window(container_name, session_name=args.tag, window_name="unsharp")
    docker_tmux_new_window(container_name, session_name=args.tag, window_name="harris")
    docker_tmux_new_window(container_name, session_name=args.tag, window_name="camera")
    docker_tmux_new_window(container_name, session_name=args.tag, window_name="resnet")
    docker_tmux_new_window(container_name, session_name=args.tag, window_name="bash")


def docker_change_submodules(container_name, repo_commit_map):
    for repo, commit in repo_commit_map.items():
        docker_git_checkout(container_name, repo, commit)


def docker_create_cgra(container_name, width=32, height=16, interconnect_only=False, dual_port=False):
    exe_cmd = ["aha", "garnet", "--verilog"]
    exe_cmd += ["--width", str(width), "--height", str(height)]
    exe_cmd += ["--use_sim_sram"]
    if interconnect_only:
        cmd += ["--interconnect-only"]
    if dual_port:
        cmd += ["--dual_port"]
        cmd += ["--mem_width", 64]
        cmd += ["--mem_depth", 512]
    docker_exec(container_name, exe_cmd)
    docker_build_simv(container_name)


def docker_build_simv(container_name):
    simv_build_dir = "/aha/garnet/tests/test_app"
    simv_build_cmd = "source /cad/modules/tcl/init/sh"
    simv_build_cmd += ";module load base vcs"
    simv_build_cmd += ";make libcgra.so"
    simv_build_cmd += ";make compile"
    exe_cmd = ["bash", "-c", f"\"{simv_build_cmd}\""]
    docker_exec(container_name, exe_cmd, wdir=simv_build_dir)


def docker_run_apps(args, tag="default_tag"):
    # run flow in tmux
    docker_tmux_exec(container_name, session_name=args.tag, window_name="gaussian", tmux_exe_cmd=f"./run_flow.sh\\ gaussian\\ {tag}")
    docker_tmux_exec(container_name, session_name=args.tag, window_name="unsharp", tmux_exe_cmd=f"./run_flow.sh\\ unsharp\\ {tag}")
    docker_tmux_exec(container_name, session_name=args.tag, window_name="harris", tmux_exe_cmd=f"./run_flow.sh\\ harris\\ {tag}")
    docker_tmux_exec(container_name, session_name=args.tag, window_name="camera", tmux_exe_cmd=f"./run_flow.sh\\ camera\\ {tag}")
    docker_tmux_exec(container_name, session_name=args.tag, window_name="resnet", tmux_exe_cmd=f"./run_flow.sh\\ resnet\\ {tag}")


if __name__=="__main__":
    # Input Arguments
    parser = argparse.ArgumentParser(description='AHA Docker Automation Flow')
    parser.add_argument("tag", type=str, help="The name of this run, it will be used to name the tmux session and tagging the aha report run")
    parser.add_argument("--github-email", type=str, default='pohan@stanford.edu', help="Github email")
    parser.add_argument("--github-name", type=str, default='Po-Han Chen', help="Github name")
    parser.add_argument("--container-image", type=str, default='stanfordaha/garnet:latest', help="Container image")
    parser.add_argument("--width", type=int, default=32, help="CGRA array width")
    parser.add_argument("--height", type=int, default=16, help="CGRA array height")
    args = parser.parse_args()

    # target container name
    whoami = os.getlogin()
    container_name = f"{whoami}-{args.tag}"

    # change submodules
    repo_commit_map = {}
    # repo_commit_map["/aha/lassen"] = "5e4a5b6" # baseline
    # repo_commit_map["/aha/lassen"] = "onyx-mac" # mac
    # repo_commit_map["/aha/lassen"] = "onyx-tadd" # tadd
    #repo_commit_map["/aha/lassen"] = "onyx-mac-tadd" # mac-tadd

    # explore
    docker_setup_container(container_name=container_name, args=args)
    docker_change_submodules(container_name=container_name, repo_commit_map=repo_commit_map)
    docker_create_cgra(container_name=container_name, width=args.width, height=args.height)
    #docker_run_apps(args=args, tag=args.tag)
    
