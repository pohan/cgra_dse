# basic setup
apt update
apt install -y vim tmux
module load base xcelium vcs

git config --global user.email "pohan@stanford.edu"
git config --global user.name "Po-Han Chen"

# install python packages
pip install Pillow-SIMD
pip install pycodestyle

# move dot files in
cp ./dotfiles/.tmux.conf ~/.tmux.conf
cp ./dotfiles/.vimrc ~/.vimrc

