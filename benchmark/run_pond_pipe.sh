# check out hte pond-pipe branch of metamapper
cd /aha/MetaMapper
git fetch
git checkout pond-pipe

# parameter
export POND_PIPELINING=1

# skip 
#aha map apps/${app_name} --width ${array_width} --height ${array_height} --log
# and do
aha pipeline apps/${app_name} --width ${array_width} --height ${array_height} --log

# how to generate the sub-graph
follow the instructions in the documents, first generate the app resutls in docker (you can use unroll=1)
and then do the analysis on kiwi (by cp-ing those files out)
if the frequent subgraph doesn't show up, increase the number to like 20, if still no then it is not frequent
