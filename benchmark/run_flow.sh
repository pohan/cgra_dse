#!/bin/bash

if [ -z "$1" ]; then
    echo "invoke the script with app name!";
    exit 1;
else
    echo "Sourcing config_${1}.cfg..."
    source ./config/config_${1}.cfg
    if [ $? -ne 0 ]; then
        exit 1;
    fi
fi

function check_cmd {
    if [ $1 -eq 0 ]; then
        echo -e "\033[32m $2 complete! \033[0m";
    else
        echo -e "\033[31m $2 errored out... \033[0m";
        exit;
    fi
}

# tag name
tag=$2

# script execution time
exe_time=`TZ=America/Los_Angeles date '+%Y%m%d-%H%M'`
bin_folder=bin-${app_name}-${tag}-${exe_time}
log_folder=log-${app_name}-${tag}-${exe_time}

# tool setup
source /cad/modules/tcl/init/sh
module load base
module load vcs
module load xcelium

# array setting
array_width=32
array_height=16

#=== parameters
export DISABLE_GP=1
export PNR_PLACER_EXP=4
# export POND_PIPELINED=1
export PIPELINED=1

#=== path setting
app_base=/aha/Halide-to-Hardware/apps/hardware_benchmarks/apps

#=== clean up previous run
make -C ${app_base}/${app_name} clean
make -C ${app_base}/${app_name} clean
make -C ${app_base}/${app_name} clean

#=== Execute
# ============================================================
# AHA Halide
aha halide apps/${app_name} --log
check_cmd $? "aha halide"

# AHA Map
aha pipeline apps/${app_name} --width ${array_width} --height ${array_height} --use_sim_sram --log
check_cmd $? "aha map"

# AHA STA
aha sta apps/${app_name} --log --visualize
check_cmd $? "aha sta"

# AHA SIM
aha glb apps/${app_name} --run --log
check_cmd $? "aha glb"

# ============================================================
# AHA Report and archieve
aha report apps/${app_name} --dump-report --tag ${tag}

# Backup the bin/ and log/
# cp -r ${app_base}/${app_name}/bin ./${bin_folder}
cp -r ${app_base}/${app_name}/log ./${log_folder}

# remove big binary files
# rm -f ./${bin_folder}/a.out
# rm -f ./${bin_folder}/${app_name}.generator
# rm -f ./${bin_folder}/clockwork_codegen
# rm -f ./${bin_folder}/process

# Archive
# tar -czf ${app_name}-${exe_time}.tar.gz ./${bin_folder} ./${log_folder}
