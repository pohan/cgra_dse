## == to switch between different PEs
# -- Magma
#cd /aha/magma
#git fetch
#git checkout master
#git pull origin master
# -- Peak
#cd /aha/peak
#git fetch
#git checkout master
#git pull origin master
# -- Lassen 
#cd /aha/lassen
#git fetch
#git checkout 5e4a5b619919aeee456042236710c7202a6f2d49 # (Baseline PE)
#git checkout e4df3e3600aee912beabffc84f9629cee63c7c62 # (TADD PE)
#git checkout 3f015a778dcd5e505a3e5592f247774ce35b7607 # (MAC PE)
#git checkout c5d074482788853d0f437124df660b63f4d48907 # (TADD+MAC PE)

## == to use pond pipelining
# archipelago
#cd /aha/archipelago
#git fetch
#git checkout pipeline-hard-flush
# clockwork
#cd /aha/clockwork
#git fetch
#git checkout new_lake
#source ./user_settings/aha_settings.sh
#make all -j8
# Halide-to-Hardware
#cd /aha/Halide-to-Hardware
#git fetch
#git checkout clockwork
#make compiler -j8
# MetaMapper
#cd /aha/MetaMapper
#git fetch
#git checkout pond-pipe
# garnet
#cd /aha/garnet/
#git fetch
#git checkout pond-pipe

## == to use dual-port memory tile
# cd /aha/garnet
# git fetch
# git checkout dse_args
# git pull origin dse_args

# == get latest halide codes, but still need to modify host_tiling by hand
#cd /aha/Halide-to-Hardware
#git fetch
#git checkout clockwork-tile-1
#make compiler -j8

# there is something wrong with the frequency
# suggested by Jack, change the garnet to latest master
# (but it failed with resnet apps)
cd /aha/garnet
git fetch
git checkout master
git pull origin master

## checkout latest report utility
cd /aha/aha/util
git fetch
git checkout origin/aha-report-util -- halide.py
git checkout origin/aha-report-util -- map.py
git checkout origin/aha-report-util -- sta.py
git checkout origin/aha-report-util -- glb.py
git checkout origin/aha-report-util -- report.py

