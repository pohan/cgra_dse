# CGRA Design Space Exploration
This repo collects the scripts for CGRA design space exploration experiments

## Initialization
Use the script `init_docker.sh` to create and initialize a docker container. To run it, simply do:

```
$ ./init_docker.sh <name-of-the-container>
```
if `<name-of-the-container>` is left empty, it will use the default container name that is hardcoded in the script.

This script does the following:

1. Create a docker container with latest image
2. Copy ssh-key and this project folder into the container
3. Initialize the container with `init_container.sh`
4. Initialize the aha submodules with `init_submodule.sh`
5. Run `aha garnet` to generate a 32x16 CGRA

Please take a look at `init_container.sh` and `init_submodule.sh` before starting because these settings often change based on users and their purposes.

## Run AHA Flow
Under construction. Basically, use the `benchmark/run_flow.sh` script to run the test like:

```
$ ./run_flow <app-name>
```
then it will first source the setting in `benchmark/config/config_<app-name>.cfg` and then run through the aha flow.
