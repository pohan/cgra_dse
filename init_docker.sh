#!/bin/bash

AHA_ENV=/aha/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [ -z "$1" ]; then
    echo "== Using default container name !!"
    my_name=cgra-dse
else
    my_name=${1}
fi

# name of container and tmux
tmux_name=$my_name
container_name=pohan-$my_name
echo "== Container Name:${container_name}"

# get latest container image
docker pull stanfordaha/garnet:latest

# create container in detached mode
docker run -it --rm -d -v /cad:/cad --name ${container_name} stanfordaha/garnet:latest bash

# copy things into the container
docker cp ~/.ssh ${container_name}:/root/
docker cp /nobackup/pohan/workspace/cgra_dse ${container_name}:/aha/

# setup the container environment
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} bash init_container.sh
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} bash init_submodule.sh

# create the CGRA
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} aha garnet --verilog --width 32 --height 16 --use_sim_sram

# build the simv
simv_build_dir=/aha/garnet/tests/test_app
simv_build_cmd="source /cad/modules/tcl/init/sh; module load base vcs; make libcgra.so; make compile"
docker exec -it -w ${simv_build_dir} --env PATH=${AHA_ENV} ${container_name} bash -c "${simv_build_cmd}"

# create tmux session in container
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} tmux new-session -n gaussian -s ${tmux_name} -c /aha/cgra_dse/benchmark -d
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} tmux new-window -n unsharp -t ${tmux_name} -c /aha/cgra_dse/benchmark
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} tmux new-window -n harris -t ${tmux_name} -c /aha/cgra_dse/benchmark
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} tmux new-window -n camera -t ${tmux_name} -c /aha/cgra_dse/benchmark
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} tmux new-window -n resnet -t ${tmux_name} -c /aha/cgra_dse/benchmark
docker exec -it -w /aha/cgra_dse --env PATH=${AHA_ENV} ${container_name} tmux new-window -n bash -t ${tmux_name} -c /aha/cgra_dse/benchmark

# run flow (in tmux)
docker exec -it --env PATH=${AHA_ENV} ${container_name} tmux send -t ${tmux_name}:gaussian.0 ./run_flow.sh\ gaussian Enter
docker exec -it --env PATH=${AHA_ENV} ${container_name} tmux send -t ${tmux_name}:unsharp.0 ./run_flow.sh\ unsharp Enter
docker exec -it --env PATH=${AHA_ENV} ${container_name} tmux send -t ${tmux_name}:harris.0 ./run_flow.sh\ harris Enter
docker exec -it --env PATH=${AHA_ENV} ${container_name} tmux send -t ${tmux_name}:camera.0 ./run_flow.sh\ camera Enter
#docker exec -it --env PATH=${AHA_ENV} ${container_name} tmux send -t ${tmux_name}:resnet.0 ./run_flow.sh\ resnet Enter

# run flow (old way)
# docker exec -it -w /aha/cgra_dse/benchmark --env PATH=${AHA_ENV} ${container_name} ./run_flow.sh gaussian
# docker exec -it -w /aha/cgra_dse/benchmark --env PATH=${AHA_ENV} ${container_name} ./run_flow.sh unsharp
# docker exec -it -w /aha/cgra_dse/benchmark --env PATH=${AHA_ENV} ${container_name} ./run_flow.sh harris
# docker exec -it -w /aha/cgra_dse/benchmark --env PATH=${AHA_ENV} ${container_name} ./run_flow.sh camera
# docker exec -it -w /aha/cgra_dse/benchmark --env PATH=${AHA_ENV} ${container_name} ./run_flow.sh resnet

